package com.example.hackaton;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Date;
import java.time.Clock;

public class BD extends SQLiteOpenHelper {

    private static final String nombre_bd = "hackday";
    private static final int version_bd = 4;
    private static final String tabla_registro = "CREATE TABLE REGISTRO(CEDULA TEXT PRIMARY KEY, NOMBRE TEXT, FECHANACIMIENTO TEXT, GENERO TEXT, CORREOELECTRONICO TEXT, CELULAR TEXT )";


    public BD (Context context){
        super(context, nombre_bd, null, version_bd);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase){
        sqLiteDatabase.execSQL(tabla_registro);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,int i , int ii){
       // sqLiteDatabase.execSQL("DROP TABLE IF EXISTS REGISTRO" + tabla_registro);
        sqLiteDatabase.execSQL(tabla_registro);
    }

    public void agregarUsuario(String cedula, String nombre, String fechaNacimiento, String genero, String correo, String celular){
        SQLiteDatabase bd = getWritableDatabase();
        if(bd!=null){
            try {
                bd.rawQuery("INSERT INTO hackday.REGISTRO VALUES('"+cedula+"','"+nombre+"', '"+fechaNacimiento+"', '"+genero+"', '"+correo+"', '"+celular+"')",null);
            } catch (SQLiteException e) {
                System.out.println(e);
            }
            bd.execSQL("SELECT * FROM REGISTRO ");
            bd.close();
        }
    }
}
