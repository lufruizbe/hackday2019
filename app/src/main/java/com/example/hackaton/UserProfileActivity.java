package com.example.hackaton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class UserProfileActivity extends AppCompatActivity {
    private Button botonGuardar;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        botonGuardar= findViewById(R.id.guardar);
        botonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturaDedatos();
            }});
    }



    private void capturaDedatos(){
        EditText nombre = findViewById(R.id.editText);
        EditText numeroDeDocumento =findViewById(R.id.cedula);
        EditText fechaNacimiento = findViewById(R.id.fechaNacimiento);
        EditText genero = findViewById(R.id.genero);
        EditText correoElectronico = findViewById(R.id.correo);
        EditText celular = findViewById(R.id.celular);

        final BD bd= new BD(getApplicationContext());

        bd.agregarUsuario(numeroDeDocumento.getText().toString(),nombre.getText().toString(),fechaNacimiento.getText().toString(),genero.getText().toString(),correoElectronico.getText().toString(),celular.getText().toString());
        Toast.makeText(getApplicationContext(), "SE AGREGO CORRECTAMENTE", Toast.LENGTH_SHORT).show();

       Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {correoElectronico.getText().toString()});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Mail from app!");
        intent.putExtra(Intent.EXTRA_TEXT, "OMFG I just sent an email from my app!");

        try {
            startActivity(Intent.createChooser(intent, "How to send mail?"));
        } catch (android.content.ActivityNotFoundException ex) {

        }
    }
}
